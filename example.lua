#!/usr/bin/env lua
local inspect = require"inspect"

RootConfig = {}
function RootConfig:new()
  local o = {}
  self.__index = self
  setmetatable(o, self)
  o.root_location = "/root"
  o.root_name = "root config"
  return o
end

function RootConfig:runRootConfig(arg)
  print("RootConfig. arg: " .. arg)
end




ProgressiveConfig = RootConfig:new()
function ProgressiveConfig:new()
  local o = {}
  self.__index = self
  setmetatable(o, self)
  o.location = "/progressives"
  o.progressive_config_name = "progressive config"
  return o
end





SoundConfig = RootConfig:new()
function SoundConfig:new()
  local o = {}
  self.__index = self
  setmetatable(o, self)
  o.location = "/sounds"
  o.sound_config_name = "sound config"
  return o
end




BaseObject = {}
function BaseObject:new(config)
  local base_obj = config:new() -- Base object inherits from a concrete config
  self.__index = self
  setmetatable(base_obj, self) -- If something is not found in base_obj, search in BaseObject table
  setmetatable(self, config) -- If something is not found in BaseObject, search in Config
  base_obj.base_name = "base object"
  return base_obj
end

function BaseObject:StartDeployment()
  print("BaseObject: start deployment")
  self:deploy()
end

function BaseObject:IAMBaseObject()
end



ProgressiveObject = BaseObject:new(ProgressiveConfig)
function ProgressiveObject:new()
  local o = {}
  self.__index = self
  setmetatable(o, self)
  o.progressive_obj_name = "progressive object"
  return o
end

function ProgressiveObject:deploy()
  print("ProgressiveObject deploying to " .. self.root_location .. self.location)
end




SoundObject = BaseObject:new(SoundConfig)
function SoundObject:new()
  local o = {}
  self.__index = self
  setmetatable(o, self)
  o.sound_obj_name = "sound object"
  return o
end

function SoundObject:deploy()
  print("SoundObject deploying to " .. self.root_location .. self.location)
end



function factory(ObjectType)
  return ObjectType:new()
end


local progressive_obj = factory(ProgressiveObject)
progressive_obj:StartDeployment()

local sound_obj = factory(SoundObject)
sound_obj:StartDeployment()


progressive_obj:StartDeployment()
sound_obj:StartDeployment()

print(inspect(progressive_obj))
print(inspect(sound_obj))
